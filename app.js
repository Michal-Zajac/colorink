const express = require('express');
const app = express();
var port = process.env.PORT || 3000;


app.set('view engine', 'ejs');

const navbarController = require('./controllers/navbar-controller');
const mainController = require('./controllers/main-controller');

app.use(express.static('public'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.get('/', function (req, res) {
    res.redirect('/pl');
  });

mainController(app);
navbarController(app);

app.get('*', (req, res) => {
    res.render("404")
  })

app.listen(port);

