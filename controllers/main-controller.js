module.exports = function(main){

    const pl = require('../language/pl.json');
    const de = require('../language/de.json');
    const en = require('../language/en.json');

    main.get('/pl/', function (req, res) {
        res.render('index', {
          metaDescription:pl.metaDescription,
          originalUrl: req.originalUrl.slice(3),
          domain: pl.domain,
          company: pl.company,
          lang: pl.lang ,
          homepage: pl.homepage,
          buy:pl.buy,
          buyDescription:pl.buyDescription,
          regeneration:pl.regeneration,
          regenerationDescription:pl.regenerationDescription,
          being:pl.being,
          ekoDescpription:pl.ekoDescpription,
          buyPrices:pl.buyPrices,
          buyPriceValue1:pl.buyPriceValue1,
          buyPriceValue2:pl.buyPriceValue2,
          buyPriceValue3:pl.buyPriceValue3,
          regenPrice:pl.regenPrice,
          regenPriceValue1:pl.regenPriceValue1,
          regenPriceValue2:pl.regenPriceValue2,
          promotion:pl.promotion,
          promotionInfo:pl.promotionInfo,
          promotionDescription1:pl.promotionDescription1,
          promotionDescription2:pl.promotionDescription2,
          promotionDescription3:pl.promotionDescription3,
          findUsHere:pl.findUsHere,
          packageAddres:pl.packageAdres
        });
      })
    main.get('/en/', function (req, res) {
        res.render('index', {
          metaDescription:en.metaDescription,
          originalUrl: req.originalUrl.slice(3),
          domain: en.domain,
          company: en.company,
          lang: en.lang ,
          homepage: en.homepage,
          metaDescription:en.metaDescription,
          originalUrl: req.originalUrl.slice(3),
          domain: en.domain,
          company: en.company,
          lang: en.lang ,
          homepage: en.homepage,
          buy:en.buy,
          buyDescription:en.buyDescription,
          regeneration:en.regeneration,
          regenerationDescription:en.regenerationDescription,
          being:en.being,
          ekoDescpription:en.ekoDescpription,
          buyPrices:en.buyPrices,
          buyPriceValue1:en.buyPriceValue1,
          buyPriceValue2:en.buyPriceValue2,
          buyPriceValue3:en.buyPriceValue3,
          regenPrice:en.regenPrice,
          regenPriceValue1:en.regenPriceValue1,
          regenPriceValue2:en.regenPriceValue2,
          promotion:en.promotion,
          promotionInfo:en.promotionInfo,
          promotionDescription1:en.promotionDescription1,
          promotionDescription2:en.promotionDescription2,
          promotionDescription3:en.promotionDescription3,
          findUsHere:en.findUsHere,
          packageAddres:en.packageAdres
        });
      });
    };