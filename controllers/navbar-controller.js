module.exports = function(nav){

    const pl = require('../language/pl.json');
    const de = require('../language/de.json');
    const en = require('../language/en.json');

    nav.get('/pl/', function (req, res) {
        res.render('navbar', {
          metaDescription:pl.metaDescription,
          keyword1:pl.keyword1,
          keyword2:pl.keyword2,
          keyword3:pl.keyword3,
          keyword4:pl.keyword4,
          keyword5:pl.keyword5,
          originalUrl: req.originalUrl.slice(3),
          domain: pl.domain,
          company: pl.company,
          lang: pl.lang ,
          homepage: pl.homepage,
        });
      })
    nav.get('/de/', function (req, res) {
        res.render('navbar', {
          metaDescription:de.metaDescription,
          keyword1:de.keyword1,
          keyword2:de.keyword2,
          keyword3:de.keyword3,
          keyword4:de.keyword4,
          keyword5:de.keyword5,
          originalUrl: req.originalUrl.slice(3),
          domain: de.domain,
          company: de.company,
          lang: de.lang ,
          homepage: de.homepage,

        });
      })
    nav.get('/en/', function (req, res) {
        res.render('navbar', {
          metaDescription:en.metaDescription,
          keyword1:en.keyword1,
          keyword2:en.keyword2,
          keyword3:en.keyword3,
          keyword4:en.keyword4,
          keyword5:en.keyword5,
          originalUrl: req.originalUrl.slice(3),
          domain: en.domain,
          company: en.company,
          lang: en.lang ,
          homepage: en.homepage,
        });
      });
    };