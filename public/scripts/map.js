// Initialize and add the map
function initMap() {
    // The location of colorink51.100361, 17.031873
    const colorink = { lat: 51.100361, lng: 17.031873 };
    // The map, centered at colorink
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 18,
      center: colorink,
    });
    // The marker, positioned at colorink
    const marker = new google.maps.Marker({
      position: colorink,
      map: map,
    });
  }
  window.initMap = initMap;